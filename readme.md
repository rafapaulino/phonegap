﻿## Estudo PhoneGap Cordova

Coloquei neste aplicativo quase todos os itens do jQuery Mobile:
[Jquery Mobile](https://jquerymobile.com/)

E também a base de comandos dos plugins do PhoneGap:
[PhoneGap Plugins](http://docs.phonegap.com/plugin-apis/)

### Comandos utilizados:
phonegap create HelloWorldPhoneGap com.rafael.hellophonegap HelloWorldPhoneGap
cordova create HelloWorldCordova com.rafael.hellocordova HelloWorldCordova

### Comandos executados para instalação de plugins:
cordova plugin add cordova-plugin-device
cordova plugin add cordova-plugin-battery-status
cordova plugin add cordova-plugin-camera
cordova plugin add cordova-plugin-console
cordova plugin add cordova-plugin-contacts
cordova plugin add cordova-plugin-device-motion
cordova plugin add cordova-plugin-device-orientation
cordova plugin add cordova-plugin-dialogs
cordova plugin add cordova-plugin-file
cordova plugin add cordova-plugin-file-transfer
cordova plugin add cordova-plugin-geolocation
cordova plugin add cordova-plugin-globalization
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-media-capture
cordova plugin add cordova-plugin-network-information
cordova plugin add cordova-plugin-splashscreen
cordova plugin add cordova-plugin-statusbar
cordova plugin add cordova-plugin-vibration


A parte da splash screen foi feita com base neste tutorial:
(http://nativebuild.com/2014/gerar-icones-splash-screen-cordova-phonegap.html)
(https://github.com/felquis/generate-splash-screen-and-icons)

### Comandos executados para o build:
cordova platforms add ios
cordova platforms add android
cordova platform add wp8

### Comandos para verficar se o ambiente pode dar o build no projeto:
javac -version
android
android avd
ant
