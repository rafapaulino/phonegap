/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var contatos = {
    // Take picture using device camera and retrieve image as base64-encoded string
    meusContatos: function() {
        var options = new ContactFindOptions();
        options.filter="";
        options.multiple=true;
        var fields = ["*"];
        navigator.contacts.find(fields, this.onSuccess, this.onError, options);
    },
    novoContato: function() {
        var myContact = navigator.contacts.create({"displayName": "Novo Contato"});
        var name = new ContactName();
        name.givenName = "Jane";
        name.familyName = "Doe";
        myContact.name = name;

        var phoneNumbers = [];
        phoneNumbers[0] = new ContactField('work', '212-555-1234', false);
        phoneNumbers[1] = new ContactField('mobile', '917-555-5432', true); // preferred number
        phoneNumbers[2] = new ContactField('home', '203-555-7890', false);
        myContact.phoneNumbers = phoneNumbers;

        myContact.note = "Exemplo de nota";

        myContact.save(this.novoCallBack, this.onError);
    },
    novoCallBack: function() {
        this.meusContatos();
    },
    editarContato: function() {
        var options = new ContactFindOptions();
        options.filter   = 'Novo Contato'; //name that you want to search
        options.multiple = false;
        var fields = ["id","displayName", "phoneNumbers"];
        navigator.contacts.find(fields, this.successEditarContato, this.onError, options);
    },
    successEditarContato: function(contacts) {

        var contact = contacts[0]; //found contact array must be one as we disabled multiple false
        // Change the contact details
        contact.phoneNumbers[0].value = "999999999";
        contact.name = 'Bob';
        contact.displayName = 'Novo Contato Atualizado';
        contact.nickname = 'Boby'; // specify both to support all devices
        contact.save(this.novoCallBack, this.onError);
    },
    deletarContato: function() {
        var options = new ContactFindOptions();
        options.filter   = 'Novo Contato'; //name that you want to search
        options.multiple = false;
        var fields = ["id","displayName", "phoneNumbers"];
        navigator.contacts.find(fields, this.successDeletarContato, this.onError, options);
    },
    successDeletarContato: function(contacts) {
            
            if (contacts.length == 0) {
                //console.log("All contacts removed");
                this.meusContatos();
                return;
            }

            var contact = contacts.pop();
            contact.remove(function() {
                this.successDeletarContato(contacts);
            }, null);
    },
    backupAllTheContacts: function() {
            navigator.contacts.find(["*"], function(contacts) {
                //console.log("contacts.length = " + contacts.length);
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                    fileSystem.root.getFile("contacts.bak", {create: true, exclusive: false}, function(fileEntry) {
                        fileEntry.createWriter(function(writer) {
                            writer.onwriteend = function() {
                                navigator.notification.alert(
                                    'O backup dos seus contatos foi realizado com sucesso!',  // message
                                    null             // callback
                                    'Backup completo',    // title
                                    'Ok'                 // buttonName
                                );
                            };
                            writer.write(JSON.stringify(contacts));
                        }, this.onError);
                    }, this.onError);
                }, this.onError);
            }, this.onError, {"multiple": true});    
    },
    // Called if something bad happens. 
    onError: function (message) {
        navigator.notification.alert(
            'O Motivo foi: '+ message, // message
            this.alertDismissed,  // callback
            'Ocorreu um erro!', // title
            'Ok' // buttonName
        );
    },
    alertDismissed: function() {
        // do something
        navigator.notification.vibrate(2000);
    },
    onSuccess: function(imageData) {

        var cl = $('#contatosLista');

        for (var i = 0; i < contacts.length; i++)
        {
            cl.append('<li class="ui-li-static ui-body-inherit">'+contacts[i].phoneNumber[0].value+'</li>');
            cl.listview('refresh');
        }
    } 
};
