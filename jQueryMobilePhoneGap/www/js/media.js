var media = {
    audio: function () {
		navigator.device.capture.captureAudio(this.captureSuccess, this.captureError, {limit:2});
    },
    image: function () {
		navigator.device.capture.captureImage(this.captureSuccess, this.captureError, {limit:2});
    },
    video: function () {
		navigator.device.capture.captureVideo(this.captureSuccess, this.captureError, {limit:2});
    },
    captureSuccess = function(mediaFiles) {
	    var i, path, len;
	    for (i = 0, len = mediaFiles.length; i < len; i += 1) {
	        path = mediaFiles[i].fullPath;
	        // do something interesting with the file
	        navigator.notification.alert('Arquivo: ' + path, null, 'Arquivos');
	    }
	},
	captureError = function(error) {
    	navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
	}
};
