/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var battery = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        confirm.receivedEvent('deviceready');
    },
    //mostra um alerta na tela falando sobre o status da bateria
    onBatteryStatus: function(info) {
        navigator.notification.alert(
            "Level: " + info.level + " isPlugged: " + info.isPlugged,  // message
            this.alertDismissed,             // callback
            'Informação sobre a bateria:',    // title
            'Ok, já entendi!'                 // buttonName
        );
    },
    //executa uma ação quando o alerta vai embora
    alertDismissed: function() {
        // do something
        navigator.notification.vibrate(5000);
    },
    onBatterySuperLow: function(info) {
        navigator.notification.alert("Your battery is SUPER low!");
    },
    onBatteryLow: function(info) {
        navigator.notification.alert("Your battery is low!");
    },
    pegarStatus: function(){
        window.addEventListener("batterystatus", this.onBatteryStatus, false);
        window.addEventListener("batterycritical", this.onBatterySuperLow, false);
        window.addEventListener("batterylow", this.onBatteryLow, false);
    } 
};
