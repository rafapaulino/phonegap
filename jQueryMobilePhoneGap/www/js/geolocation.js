var geo = {
    onError: function(message) {
        navigator.notification.alert(
            'O Motivo foi: '+ message, // message
            this.alertDismissed,  // callback
            'Ocorreu um erro ao acessar a sua camera!', // title
            'Ok' // buttonName
        );
    },
    verLocatiozacao: function() {
        navigator.geolocation.getCurrentPosition(this.onSuccess, this.onError);
    },
    onSuccess: function(position) {
        
        navigator.notification.alert(
            'Latitude: '          + position.coords.latitude          + '\n' +
            'Longitude: '         + position.coords.longitude         + '\n' +
            'Altitude: '          + position.coords.altitude          + '\n' +
            'Accuracy: '          + position.coords.accuracy          + '\n' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
            'Heading: '           + position.coords.heading           + '\n' +
            'Speed: '             + position.coords.speed             + '\n' +
            'Timestamp: '         + position.timestamp                + '\n', // message
            null,  // callback
            'Confira a sua localização', // title
            'Ok' // buttonName
        );
    }
};
