var globalization = {
    idioma: function () {
        navigator.globalization.getPreferredLanguage(
            
            function (language) {
                alert('language: ' + language.value + '\n');
            },
            function () {
                alert('Error getting language\n');
            }
        );
    },
    local: function () {
        navigator.globalization.getLocaleName(
            
            function (locale) {
                alert('locale: ' + locale.value + '\n');
            },
            function () {
                alert('Error getting locale\n');
            }
        );
    },
    data: function () {
        navigator.globalization.dateToString(
            
            new Date(),
            function (date) {
                alert('date: ' + date.value + '\n');
            },
            function () {
                alert('Error getting date\n');
            },
            { formatLength: 'short', selector: 'date and time' }
        );
    },
    moeda: function () {
        navigator.globalization.getCurrencyPattern(
            'USD',
            function (pattern) {
                alert('pattern: '  + pattern.pattern  + '\n' +
                      'code: '     + pattern.code     + '\n' +
                      'fraction: ' + pattern.fraction + '\n' +
                      'rounding: ' + pattern.rounding + '\n' +
                      'decimal: '  + pattern.decimal  + '\n' +
                      'grouping: ' + pattern.grouping);
            },
            function () { alert('Error getting pattern\n'); }
        );
    },
    nomeDatas: function () {
        navigator.globalization.getDateNames(
            function (names) {
                for (var i = 0; i < names.value.length; i++) {
                    alert('month: ' + names.value[i] + '\n');
                }
            },
            function () { alert('Error getting names\n'); },
            { type: 'wide', item: 'months' }
        );
    },
    checkDatePattern: function () {
        navigator.globalization.getDatePattern(
            function (date) { alert('pattern: ' + date.pattern + '\n'); },
            function () { alert('Error getting pattern\n'); },
            { formatLength: 'short', selector: 'date and time' }
        );
    },
    primeiroDiaSemana: function () {
        navigator.globalization.getFirstDayOfWeek(
            function (day) {alert('day: ' + day.value + '\n');},
            function () {alert('Error getting day\n');}
        );
    },
    numberPattern: function () {
        navigator.globalization.getNumberPattern(
            function (pattern) {alert('pattern: '  + pattern.pattern  + '\n' +
                                      'symbol: '   + pattern.symbol   + '\n' +
                                      'fraction: ' + pattern.fraction + '\n' +
                                      'rounding: ' + pattern.rounding + '\n' +
                                      'positive: ' + pattern.positive + '\n' +
                                      'negative: ' + pattern.negative + '\n' +
                                      'decimal: '  + pattern.decimal  + '\n' +
                                      'grouping: ' + pattern.grouping);},
            function () {alert('Error getting pattern\n');},
            {type:'decimal'}
        );
    },
    dayLightSavingsTime: function () {
        navigator.globalization.isDayLightSavingsTime(
            new Date(),
            function (date) {alert('dst: ' + date.dst + '\n');},
            function () {alert('Error getting names\n');}
        );
    },
    numberToString: function () {
        navigator.globalization.numberToString(
            3.1415926,
            function (number) {alert('number: ' + number.value + '\n');},
            function () {alert('Error getting number\n');},
            {type:'decimal'}
        );
    },
    stringToDate: function () {
        navigator.globalization.stringToDate(
            '9/25/2012',
            function (date) {alert('month:' + date.month +
                                   ' day:'  + date.day   +
                                   ' year:' + date.year  + '\n');},
            function () {alert('Error getting date\n');},
            {selector: 'date'}
        );
    },
    stringToNumber: function () {
        navigator.globalization.stringToNumber(
            '1234.56',
            function (number) {alert('number: ' + number.value + '\n');},
            function () {alert('Error getting number\n');},
            {type:'decimal'}
        );
    }
};
