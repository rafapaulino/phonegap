/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var camera = {
    // Take picture using device camera and retrieve image as base64-encoded string
    capturePhoto: function() {

        navigator.camera.getPicture(this.onSuccess, this.onFail, { 
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.DATA_URL
        });
    },
    // Take picture using device camera and retrieve image as base64-encoded string
    captureAlbum: function() {

        navigator.camera.getPicture(this.onSuccess, this.onFail, { 
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        });
    },
    // Called if something bad happens. 
    onFail: function (message) {
        navigator.notification.alert(
            'O Motivo foi: '+ message, // message
            this.alertDismissed,  // callback
            'Ocorreu um erro ao acessar a sua camera!', // title
            'Ok' // buttonName
        );
    },
    //executa uma aÃ§Ã£o quando o alerta vai embora
    alertDismissed: function() {
        // do something
        navigator.notification.vibrate(2000);
    },
    //quando pegar a foto
    onSuccess: function(imageData) {

        var largeImage = document.getElementById('largeImage');
        // Unhide image elements
        largeImage.style.display = 'block';

        // Show the captured photo
        // The inline CSS rules are used to resize the image
        largeImage.src = "data:image/jpeg;base64," + imageData;
    } 
};
