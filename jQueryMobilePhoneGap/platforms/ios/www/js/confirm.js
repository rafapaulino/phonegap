/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var confirm = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        confirm.receivedEvent('deviceready');
    },
    //mostra um alerta na tela
    showAlert: function() {
        navigator.notification.alert(
            'Essa é a mensagem no alerta!',  // message
            this.alertDismissed,             // callback
            'Este é o título no alerta!',    // title
            'Texto do Botão'                 // buttonName
        );
    },
    //executa uma ação quando o alerta vai embora
    alertDismissed: function() {
        // do something
        navigator.notification.vibrate(2000);
    },
    //deveria dar o beep 3 vezes
    playBeep: function() {
        navigator.notification.beep(3);
    },
    //vibra durante 2 segundos
    vibrate: function() {
        navigator.notification.vibrate(2000);
    },
    // process the promptation dialog result
    onPrompt: function(results) {
        alert("Você selecionou o botão de número: " + results.buttonIndex + " e digitou: " + results.input1);
    },
    //exibe uma caixa de dialogo
    showPrompt: function() {
        navigator.notification.prompt(
            'Por favor coloque o seu nome',  // message
            this.onPrompt,                  // callback to invoke
            'Registro',            // title
            ['Ok','Sair'],             // buttonLabels
            'Coloque o seu nome aqui...' // defaultText
        );
    }    
};
