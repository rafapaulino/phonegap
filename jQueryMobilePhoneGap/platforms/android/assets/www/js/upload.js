var upload = {
    onError: function(e) {
        console.log("FileSystem Error");
    },
    getImage: function() {
        // Retrieve image file location from specified source
        navigator.camera.getPicture(this.uploadPhoto, function(message) {
            alert('get picture failed');
            },{
                quality: 50, 
                destinationType: navigator.camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
        });
    },
    uploadPhoto: function(imageURI) {
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/')+1);
        options.mimeType = "image/jpeg";

        var params = new Object();
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        var ft = new FileTransfer();
        ft.upload(imageURI, "http://yourdomain.com/upload.php", win, this.onError, options);
    },
    win: function(r) {
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
        alert(r.response);
    } 
};
